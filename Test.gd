#
# MIDI Input
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 Leandro Motta Barros
#

extends SceneTree

var MIDIInput
var Assert

func _init():
	MIDIInput = preload("res://MIDIInput.gd")
	Assert = preload("res://Assert.gd")

	TestIsPlayingNoteSimple()
	TestIsPlayingNoteWithOtherNotes()
	TestIsPlayingMajorTriad()
	TestIsPlayingMinorTriadSimple()
	TestIsPlayingAugmentedTriadSimple()
	TestIsPlayingDiminishedTriadSimple()
	TestEmulatedKeyboardSimple()

	TestNoteName()

	quit()


# Simple test case for isPlayingNote().
func TestIsPlayingNoteSimple() -> void:
	var mi = MIDIInput.new()

	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.F7))

	mi.handleEvent(makeMIDIEvent(true, 101)) # F7
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.F7))

	mi.handleEvent(makeMIDIEvent(false, 101)) # F7
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.F7))

	mi._exit_tree()


# Tests isPlayingNote() when other notes are being played in addition to the
# desired note.
func TestIsPlayingNoteWithOtherNotes() -> void:
	var mi = MIDIInput.new()

	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.BF2))

	mi.handleEvent(makeMIDIEvent(true, 47)) # B2
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.BF2))
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.B2))

	mi.handleEvent(makeMIDIEvent(true, 46)) # B♭2
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.BF2))
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.B2))

	mi.handleEvent(makeMIDIEvent(false, 47)) # B2
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.BF2))
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.B2))

	mi.handleEvent(makeMIDIEvent(false, 46)) # B♭2
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.BF2))
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.B2))

	mi._exit_tree()


# Tests various key configurations for isPlayingMajorTriad().
func TestIsPlayingMajorTriad() -> void:
	var mi = MIDIInput.new()

	# Initially, not playing the chord
	Assert.isFalse(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	# Play note by note, until completing the chord
	mi.handleEvent(makeMIDIEvent(true, 70)) # A♯4
	Assert.isFalse(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	mi.handleEvent(makeMIDIEvent(true, 73)) # C♯5
	Assert.isFalse(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	mi.handleEvent(makeMIDIEvent(true, 66)) # F♯4
	Assert.isTrue(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	# Pressing notes outside of the "chord range" is OK: still playing the chord
	mi.handleEvent(makeMIDIEvent(true, 65)) # F4
	mi.handleEvent(makeMIDIEvent(true, 108)) # C8
	Assert.isTrue(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	# But pressing a note from the "chord range" is not OK
	mi.handleEvent(makeMIDIEvent(true, 69)) # A4
	Assert.isFalse(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	# Release this last bad note and the chord is good again
	mi.handleEvent(makeMIDIEvent(false, 69)) # A4
	Assert.isTrue(mi.isPlayingMajorTriad(MIDIInput.Note.FS4))

	mi._exit_tree()


# Simple test case for isPlayingMinorTriad().
func TestIsPlayingMinorTriadSimple() -> void:
	var mi = MIDIInput.new()

	Assert.isFalse(mi.isPlayingMinorTriad(MIDIInput.Note.E6))

	mi.handleEvent(makeMIDIEvent(true, 88)) # E6
	mi.handleEvent(makeMIDIEvent(true, 91)) # G6
	mi.handleEvent(makeMIDIEvent(true, 95)) # B6
	Assert.isTrue(mi.isPlayingMinorTriad(MIDIInput.Note.E6))

	mi._exit_tree()


# Simple test case for isPlayingAugmentedTriad().
func TestIsPlayingAugmentedTriadSimple() -> void:
	var mi = MIDIInput.new()

	Assert.isFalse(mi.isPlayingAugmentedTriad(MIDIInput.Note.GF1))

	mi.handleEvent(makeMIDIEvent(true, 30)) # G♭1
	mi.handleEvent(makeMIDIEvent(true, 34)) # B♭1
	mi.handleEvent(makeMIDIEvent(true, 38)) # D2
	Assert.isTrue(mi.isPlayingAugmentedTriad(MIDIInput.Note.GF1))

	mi._exit_tree()


# Simple test case for isPlayingDiminishedTriad().
func TestIsPlayingDiminishedTriadSimple() -> void:
	var mi = MIDIInput.new()

	Assert.isFalse(mi.isPlayingDiminishedTriad(MIDIInput.Note.GF5))

	mi.handleEvent(makeMIDIEvent(true, 78)) # G♭5
	mi.handleEvent(makeMIDIEvent(true, 81)) # A5
	mi.handleEvent(makeMIDIEvent(true, 84)) # C6
	Assert.isTrue(mi.isPlayingDiminishedTriad(MIDIInput.Note.GF5))

	mi._exit_tree()


# Simple test case for the emulated keyboard.
func TestEmulatedKeyboardSimple() -> void:
	var mi = MIDIInput.new()

	# Initially note is not pressed
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.D4))

	# Pressing the right key shall not work, as keyboard emulation is not enabled
	mi.handleEvent(makeKeyEvent(true, KEY_W))
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.D4))
	mi.handleEvent(makeKeyEvent(false, KEY_W))

	# Enable the emulation and try again: now it must play the note
	mi.setEmulatedKeyboard(true)
	Assert.isFalse(mi.isPlayingNote(MIDIInput.Note.D4))
	mi.handleEvent(makeKeyEvent(true, KEY_W))
	Assert.isTrue(mi.isPlayingNote(MIDIInput.Note.D4))

	mi._exit_tree()


# Checks if noteName() works as expected.
func TestNoteName() -> void:
	var mi = MIDIInput.new()

	Assert.isEqual("F1", mi.noteName(MIDIInput.Note.F1))
	Assert.isEqual("C3", mi.noteName(MIDIInput.Note.C3))
	Assert.isEqual("F♯5/G♭5", mi.noteName(MIDIInput.Note.FS5))
	Assert.isEqual("F♯5/G♭5", mi.noteName(MIDIInput.Note.GF5))
	Assert.isEqual("A8", mi.noteName(MIDIInput.Note.A8))

	mi._exit_tree()


# Helper to create MIDI events.
func makeMIDIEvent(noteOn: bool, pitch: int) -> InputEventMIDI:
	var event := InputEventMIDI.new()
	event.message = MIDI_MESSAGE_NOTE_ON if noteOn else MIDI_MESSAGE_NOTE_OFF
	event.pitch = pitch
	return event


# Helper to create keyboard events.
func makeKeyEvent(keyDown: bool, scancode: int) -> InputEventKey:
	var event := InputEventKey.new()
	event.pressed = keyDown
	event.scancode = scancode
	return event
