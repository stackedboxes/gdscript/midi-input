#
# MIDI Input
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 Leandro Motta Barros
#

extends Node


# These are all the notes that MIDI supports and that have a name using whatever
# naming standard I am using here. These constants use and "S" suffix to denote
# sharps and a (possibly confusing) "F" prefix to denote flats.
#
# The values match the MIDI note number (the same as the `pitch` property in
# Godot's `InputEventMIDI`).
enum Note {
	C0 =  12, CS0 =  13, DF0 =  13, D0  =  14, DS0 =  15, EF0 =  15,
	E0 =  16, F0  =  17, FS0 =  18, GF0 =  18, G0  =  19, GS0 =  20, AF0 =  20,
	A0 =  21, AS0 =  22, BF0 =  22, B0  =  23,

	C1 =  24, CS1 =  25, DF1 =  25, D1  =  26, DS1 =  27, EF1 =  27,
	E1 =  28, F1  =  29, FS1 =  30, GF1 =  30, G1  =  31, GS1 =  32, AF1 =  32,
	A1 =  33, AS1 =  34, BF1 =  34, B1  =  35,

	C2 =  36, CS2 =  37, DF2 =  37, D2  =  38, DS2 =  39, EF2 =  39,
	E2 =  40, F2  =  41, FS2 =  42, GF2 =  42, G2  =  43, GS2 =  44, AF2 =  44,
	A2 =  45, AS2 =  46, BF2 =  46, B2  =  47,

	C3 =  48, CS3 =  49, DF3 =  49, D3  =  50, DS3 =  51, EF3 =  51,
	E3 =  52, F3  =  53, FS3 =  54, GF3 =  54, G3  =  55, GS3 =  56, AF3 =  56,
	A3 =  57, AS3 =  58, BF3 =  58, B3  =  59,

	C4 =  60, CS4 =  61, DF4 =  61, D4  =  62, DS4 =  63, EF4 =  63,
	E4 =  64, F4  =  65, FS4 =  66, GF4 =  66, G4  =  67, GS4 =  86, AF4 =  68,
	A4 =  69, AS4 =  70, BF4 =  70, B4  =  71,

	C5 =  72, CS5 =  73, DF5 =  73, D5  =  74, DS5 =  75, EF5 =  75,
	E5 =  76, F5  =  77, FS5 =  78, GF5 =  78, G5  =  79, GS5 =  80, AF5 =  80,
	A5 =  81, AS5 =  82, BF5 =  82, B5  =  83,

	C6 =  84, CS6 =  85, DF6 =  85, D6  =  86, DS6 =  87, EF6 =  87,
	E6 =  88, F6  =  89, FS6 =  90, GF6 =  90, G6  =  91, GS6 =  92, AF6 =  92,
	A6 =  93, AS6 =  94, BF6 =  94, B6  =  95,

	C7 =  96, CS7 =  97, DF7 =  97, D7  =  98, DS7 =  99, EF7 =  99,
	E7 = 100, F7  = 101, FS7 = 102, GF7 = 102, G7  = 103, GS7 = 104, AF7 = 104,
	A7 = 105, AS7 = 106, BF7 = 106, B7  = 107,

	C8 = 108, CS8 = 109, DF8 = 109, D8  = 110, DS8 = 111, EF8 = 111,
	E8 = 112, F8  = 113, FS8 = 114, GF8 = 114, G8  = 115, GS8 = 116, AF8 = 116,
	A8 = 117, AS8 = 118, BF8 = 118, B8  = 119,

	C9 = 120, CS9 = 121, DF9 = 121, D9  = 122, DS9 = 123, EF9 = 123,
	E9 = 124, F9  = 125, FS9 = 126, GF9 = 126, G9  = 127
}


# Initializes the MIDIInput. Calls `OS.open_midi_inputs()`, so you don't have
# to.
func _init():
	OS.open_midi_inputs()

	_noteOn.resize(128)
	for i in range(128):
		_noteOn[i] = false


# Finalizes the MIDIInput. Calls `OS.close_midi_inputs()`, so be sure to not use
# any MIDI input stuff after your `MIDIInput` is removed from the scene tree.
# (Usually, you'll configure MIDIInput as a Godot singleton, so this should just
# work.)
func _exit_tree():
	OS.close_midi_inputs()


# Handles MIDI-related events. Call this from your code so that the MIDIInput
# module knows what MIDI events (either true or emulated) are being generated.
func handleEvent(event: InputEvent) -> void:
	if event is InputEventMIDI:
		_handleRealMIDIEvent(event as InputEventMIDI)
	elif _emulateKeyboard && event is InputEventKey:
		_handleEmulatedMIDIEvent(event as InputEventKey)


# Enables or disables keyboard emulation, which allows to use your regular
# computer keyboard as a two-octave music keyboard.
#
# Currently this is expected to work only with QWERTY layouts. Sorry for users
# of QWERTZ, Dvorak and other arguably superior layouts.
#
# In the emulated keyboard, keys from Q to U represent the white keys C4 to B4.
# The row of numeric keys are used as the black keys (for example, with the key
# 3 being the note D♯4/E♭4). Likewise, the row from Z to M represent the white
# keys C3 to B3 and the row above it represent the black keys.
#
# Notice that most computer keyboards are not good at handling several
# simultaneous key presses so this might work for simple notes but is likely to
# fail for fancy chords.
func setEmulatedKeyboard(enable: bool) -> void:
	_emulateKeyboard = enable


# Checks if a given note is currently being played.
func isPlayingNote(note: int) -> bool:
	if note < 0 || note > 127:
		return false
	return _noteOn[note]


# Checks if a major triad chord rooted at `rootNote` is currently being played.
func isPlayingMajorTriad(rootNote: int) -> bool:
	return isPlayingChord(rootNote, [0, 4, 7])


# Checks if a minor triad chord rooted at `rootNote` is currently being played.
func isPlayingMinorTriad(rootNote: int) -> bool:
	return isPlayingChord(rootNote, [0, 3, 7])


# Checks if an augmented triad chord rooted at `rootNote` is currently being
# played.
func isPlayingAugmentedTriad(rootNote: int) -> bool:
	return isPlayingChord(rootNote, [0, 4, 8])


# Checks if a diminished triad chord rooted at `rootNote` is currently being
# played.
func isPlayingDiminishedTriad(rootNote: int) -> bool:
	return isPlayingChord(rootNote, [0, 3, 6])


# Returns the name of a given note. Returns an empty string if the given note is
# invalid or does not have a name.
func noteName(note: int) -> String:
	return _noteNames.get(note, "")


# Checks if a given chord is being played. The chord is represented as a base
# note and an array of "deltas" (or "offsets") from this base note counted in
# semi-tones). The base note must be included this array, so the first element
# of deltas will always be 0. And the deltas must be in ascending order.
#
# An example may help: a major triad chord is represented using the array
# [0, 4, 7], because it is composed of three notes: the base note, a second note
# four semi-tones higher than the base note, and a third note seven semi-tones
# higher than the base note.
#
# What this function *really* does also deserves some words. It just checks if
# all the notes within the interval between the root note and the highest note
# in the chord are all in the expected state (that is, all notes in delta are
# being played, all the others are not). This is a compromise that intends to
# check if the desired chord is being correctly played, while not interfering
# with other chords that are being played at the same time.
func isPlayingChord(rootNote: int, deltas: Array) -> bool:
	# Index into the deltas array
	var d := 0

	for i in range(rootNote, rootNote + deltas.back() + 1):
		# If the chord expects a note that is out-of-range, it is not being played
		if i < 0 || i > 127:
			return false

		# Check if the i-th note should be played in this chord
		var shouldBeOn: bool = i == rootNote + deltas[d]
		d += 1 if shouldBeOn else 0

		# If the expected note state is not the expected, the chord is not being played
		if _noteOn[i] != shouldBeOn:
			return false

	# Nothing wrong found, so we assume the chord is being played
	return true


#
# Private stuff
#

# An array of Booleans, indexed by `Note`s. Tells if the corresponding note is
# on.
var _noteOn = []

# Use the computer keyboard to emulate a music keyboard?
var _emulateKeyboard = false

# This maps notes to their names. Here I use the proper Unicode characters to
# represent sharps and flats.
const _noteNames = {
	Note.C0: "C0", Note.CS0: "C♯0/D♭0", Note.D0: "D0", Note.DS0: "D♯0/E♭0",
	Note.E0: "E0", Note.F0 : "F0", Note.FS0: "F♯0/G♭0", Note.G0: "G0", Note.GS0: "G♯0/A♭0",
	Note.A0: "A0", Note.AS0: "A♯0/B♭0", Note.B0: "B0",

	Note.C1: "C1", Note.CS1: "C♯1/D♭1",  Note.D1: "D1", Note.DS1: "D♯1/E♭1",
	Note.E1: "E1", Note.F1 : "F1", Note.FS1: "F♯1/G♭1", Note.G1: "G1", Note.GS1: "G♯1/A♭1",
	Note.A1: "A1", Note.AS1: "A♯1/B♭1", Note.B1: "B1",

	Note.C2: "C2", Note.CS2: "C♯2/D♭2",  Note.D2: "D2", Note.DS2: "D♯2/E♭2",
	Note.E2: "E2", Note.F2 : "F2", Note.FS2: "F♯2/G♭2", Note.G2: "G2", Note.GS2: "G♯2/A♭2",
	Note.A2: "A2", Note.AS2: "A♯2/B♭2", Note.B2: "B2",

	Note.C3: "C3", Note.CS3: "C♯3/D♭3",  Note.D3: "D3", Note.DS3: "D♯3/E♭3",
	Note.E3: "E3", Note.F3 : "F3", Note.FS3: "F♯3/G♭3", Note.G3: "G3", Note.GS3: "G♯3/A♭3",
	Note.A3: "A3", Note.AS3: "A♯3/B♭3", Note.B3: "B3",

	Note.C4: "C4", Note.CS4: "C♯4/D♭4",  Note.D4: "D4", Note.DS4: "D♯4/E♭4",
	Note.E4: "E4", Note.F4 : "F4", Note.FS4: "F♯4/G♭4", Note.G4: "G4", Note.GS4: "G♯4/A♭4",
	Note.A4: "A4", Note.AS4: "A♯4/B♭4", Note.B4: "B4",

	Note.C5: "C5", Note.CS5: "C♯5/D♭5",  Note.D5: "D5", Note.DS5: "D♯5/E♭5",
	Note.E5: "E5", Note.F5 : "F5", Note.FS5: "F♯5/G♭5", Note.G5: "G5", Note.GS5: "G♯5/A♭5",
	Note.A5: "A5", Note.AS5: "A♯5/B♭5", Note.B5: "B5",

	Note.C6: "C6", Note.CS6: "C♯6/D♭6",  Note.D6: "D6", Note.DS6: "D♯6/E♭6",
	Note.E6: "E6", Note.F6 : "F6", Note.FS6: "F♯6/G♭6", Note.G6: "G6", Note.GS6: "G♯6/A♭6",
	Note.A6: "A6", Note.AS6: "A♯6/B♭6", Note.B6: "B6",

	Note.C7: "C7", Note.CS7: "C♯7/D♭7",  Note.D7: "D7", Note.DS7: "D♯7/E♭7",
	Note.E7: "E7", Note.F7 : "F7", Note.FS7: "F♯7/G♭7", Note.G7: "G7", Note.GS7: "G♯7/A♭7",
	Note.A7: "A7", Note.AS7: "A♯7/B♭7", Note.B7: "B7",

	Note.C8: "C8", Note.CS8: "C♯8/D♭8",  Note.D8: "D8", Note.DS8: "D♯8/E♭8",
	Note.E8: "E8", Note.F8 : "F8", Note.FS8: "F♯8/G♭8", Note.G8: "G8", Note.GS8: "G♯8/A♭8",
	Note.A8: "A8", Note.AS8: "A♯8/B♭8", Note.B8: "B8",

	Note.C9: "C9", Note.CS9: "C♯9/D♭9",  Note.D9: "D9", Note.DS9: "D♯9/E♭9",
	Note.E9: "E9", Note.F9 : "F9", Note.FS9: "F♯9/G♭9", Note.G9: "G9"
}

# Maps key scan codes to notes. Used with keyboard emulation.
const _emulatedKeys = {
	KEY_Q: Note.C4, KEY_W: Note.D4, KEY_E: Note.E4, KEY_R: Note.F4, KEY_T: Note.G4, KEY_Y: Note.A4, KEY_U: Note.B4,
	KEY_2: Note.CS4, KEY_3: Note.DS4, KEY_5: Note.FS4, KEY_6: Note.GS4, KEY_7: Note.AS4,

	KEY_Z: Note.C3, KEY_X: Note.D3, KEY_C: Note.E3, KEY_V: Note.F3, KEY_B: Note.G3, KEY_N: Note.A3, KEY_M: Note.B3,
	KEY_S: Note.CS3, KEY_D: Note.DS3, KEY_G: Note.FS3, KEY_H: Note.GS3, KEY_J: Note.AS3
}


# Handles a real MIDI event.
func _handleRealMIDIEvent(event: InputEventMIDI) -> void:
	if event.message == MIDI_MESSAGE_NOTE_ON:
		_noteOn[event.pitch] = true
	if event.message == MIDI_MESSAGE_NOTE_OFF:
		_noteOn[event.pitch] = false


# Handles an emulated MIDI event (which means it is really just a regular
# keyboard key event).
func _handleEmulatedMIDIEvent(event: InputEventKey) -> void:
	if event.echo:
		return

	var note := _emulatedKeys.get(event.scancode, -1) as int
	if note < 0:
		return

	_noteOn[note] = event.pressed
