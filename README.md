# MIDI Input

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

Helpers to deal with MIDI Input. For example, this allows you to check if the
player currently playing a certain chord.

Basic usage:

1. Go to your project settings, *AutoLoad* tab, and add `MIDIInput.gd` as a
   singleton.
2. In your `_unhandled_input()` method, add a call
   `MIDIInput.handleEvent(event)`.
3. Optionally, call `MIDIInput.setEmulatedKeyboard(true)` if you want to enable
   the use of your computer keyboard as an emulated two-octave music keyboard.
4. Call other `MIDIInput` functions, as desired. For example, to check if a
   major chord rooted at B5 is being played, call
   `MIDIInput.isPlayingMajorTriad(MIDIInput.Note.B5)`.

I feel that I would be dishonest if I didn't mention my lack of musical
knowledge. In the last 25 years or so, my only accomplishments in music were
playing the kazoo and watching a basic piano tutorial on YouTube while playing
with my new, toyish, two-octave MIDI keyboard. While I wrote this library with
the best of intentions, my disqualification may prevail. Good luck on using it
(and suggestions are always welcome!).
